#include<iostream>
#include"Matrici.h"
using namespace std;
// alocarea dinamica uni tablou, si afisarea acestuia.  
int main()
{
	int nr_lini, nr_coloane;  //se declara numaru de lini si de coloane. 
	int**p=0; //se declara tablou. Echivalten cu p[][].
	cout << "Introduceti nr de coloane= ";cin >> nr_coloane;
	cout << "Introduceti nr de linii= ";cin >> nr_lini; //Citirea nr de linii si coloane. 
	p = new int*[nr_coloane]; // Se aloca dinamic nr de coloane introduse. Echivalent cu scrierea p[nr_coloane][]. 
	for (int i = 0;i < nr_coloane;i++)
	{
		*(p + i) = new int[nr_lini]; // Pentru fiecare element care repezinta o coloana, alocam numarul de linii. Se scrie (p+i), ca sa se ajunga la umratoarea coloana.  
	}
	AfisareMat(p,nr_coloane, nr_lini); // Functie de afisare a tabloului creat. 
}

